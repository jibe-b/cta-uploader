## Dependencies

express

formidable

Run

> npm install

## User guide

Execute

> node app.js

upload page is accessible at

http://localhost:3000

or http://IPADDRESS:3000

Uploaded files are available in the _uploads/_ folder.


